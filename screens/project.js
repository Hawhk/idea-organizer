import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, FlatList, SectionList} from 'react-native';
import globalStyle from '../common/globalStyle'
import { FirebaseHOK } from '../config/firebase'
import { MaterialIcons } from '@expo/vector-icons'
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as ImagePicker from 'expo-image-picker';
import DialogInput from 'react-native-dialog-input';
import Idea from '../items/idea';
import randomId from '../common/random';

export default FirebaseHOK(Project);
function Project({route, firebase,  navigation:{navigate}}) {
    
    const params = route.params;
    const userId = firebase.auth().currentUser.uid;
    const projectsRefPath = 'users/' + userId + '/projects/' + params.key + '/';
    const projectDetails = firebase.database().ref(projectsRefPath);
    const [render, setRender] = useState(true);
    const [categories, setCategories] = useState({});
    const [isDialogVisible, setIsDialogVisible] = useState({bool: false})
    const data = {uid: userId, path: projectsRefPath, refrech: setRender}

    // cammera stuff
    // const [image, setImage] = useState(null);
    
    useEffect(() => {
        (async () => {
          if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestCameraRollPermissionsAsync();
            if (status !== 'granted') {
              alert('Sorry, we need camera roll permissions to make this work!');
            }
          }
        })();
    }, []);
    
    
    if (render){
        projectDetails.once('value', (snapshot) => {
            setCategories(snapshot.val().categories);
        });
        setRender(false);
    }
    
    let categorieKeys = [];
    let items = [];

    if (!!categories) {
        categorieKeys = Object.keys(categories);
    }

    categorieKeys.forEach((categorieKey) => {
        let keys = Object.keys(categories[categorieKey]['ideas']);
        let ideas = [];

        keys.forEach((key) => {
            ideas.push({...categories[categorieKey]['ideas'][key], key: key, catKey:categorieKey });
        });

        let dataPoints = [{list: ideas, key: categorieKey}]

        items.push({data: dataPoints, title: categorieKey, key: categorieKey});
    });

    const createIdea = async (type, filename, name, cat='main', func = () => {}) => {
        if (cat === '') {
            cat = 'main';
        }
        let ideasRef = await firebase.database().ref(projectsRefPath + `categories/${cat}/ideas/`).push();
        await ideasRef.set({
            title: name,
            content: filename,
            type: type
        });
        await uppdateProject();
        setRender(true);
        func();
    }

    const uppdateProject = async () => {
        let update = {};
        update[projectsRefPath + "edited"] = new Date();

        await firebase.database().ref().update(update);
    }

    const getPicture = async (name) => {
        
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [1, 1],
          quality: 1,
        });

        setIsDialogVisible({bool: false});
    
        if (!result.cancelled) {
            uploadImage(result.uri).then((fileName) => {
                createIdea('photo', fileName, name);
            }).catch(err => {
                console.error(err);
            })
        }
    };

    const uploadImage = async (uri) => {
        const response = await fetch(uri);
        const blob = await response.blob();

        let imgName = randomId();

        let ref = firebase.storage().ref().child('users/' + userId + '/images/' + imgName + ".jpeg");
        await ref.put(blob);
        return imgName
    }

    const mic = () => {
        navigate('Recorder', {...data, create: createIdea});
    }

    const text = (title) => {
        setIsDialogVisible({bool:false});
        navigate('Text', {...data, title: title, create: createIdea});
    }

    const renderList = ({ item }) => {
        return(
            <View>
                <FlatList
                    numColumns={2}
                    data={item.list}
                    renderItem={({ item }) => <Idea item={item} uid={userId} uri={projectsRefPath} refrech={setRender} cat={categorieKeys} create={createIdea}/>}
                />
            </View>
        )
    };

    return (
        <View style={styles.container}>
            <Text style={globalStyle.text}>{params.name}</Text>
            <View style={styles.list}>
                <SectionList 
                    onRefresh={() => setRender(true)}
                    refreshing={render}
                    sections={items}
                    renderItem={renderList}
                    ListEmptyComponent={() => (
                        <View style={styles.container}> 
                            <Text>This looks empty... try to add something</Text> 
                        </View>
                    )}
                    renderSectionHeader={({ section: { title } }) => (
                        <View style={{flex: 1, alignItems: 'center', backgroundColor: '#eee'}}> 
                            <Text style={{fontSize:40, textDecorationLine: 'underline',}}>{title.charAt(0).toUpperCase() + title.slice(1)}</Text>
                        </View>
                    )}
                />
            </View>
            <View style={styles.footer}>
                <TouchableOpacity style={styles.footerButons} onPress={() => setIsDialogVisible({bool: true, run: getPicture})}>
                    <MaterialIcons name='camera-alt' size={40} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.footerButons} onPress={mic}>
                    <MaterialIcons name='mic' size={40} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.footerButons} onPress={() => setIsDialogVisible({bool: true, run: text})}>
                    <MaterialIcons name='short-text' size={40} />
                </TouchableOpacity>

                <DialogInput isDialogVisible={isDialogVisible.bool}
                    title={"Set title"}
                    hintInput ={"title"}
                    submitInput={ (inputText) => {isDialogVisible.run(inputText)}}
                    closeDialog={ () => {setIsDialogVisible({bool: false})}}>
                </DialogInput>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eee',
    },
    footer: {
        flex: 1,
        backgroundColor: 'red',
        flexDirection: "row"
    },
    footerButons: {
        marginHorizontal: 40,
        marginVertical: 20,
        flex: 1
    },
    list: {
        flex: 5
    }, 
});