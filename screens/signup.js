import React, { useState } from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import { FirebaseHOK } from '../config/firebase'

import Button from '../common/button'

export default FirebaseHOK(SignUp);

function SignUp({firebase}) {
    
    const [userName, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setPassword2] = useState('');
    const [len, setLen] = useState(0);
    const [match, setMatch] = useState(true);
    const [err, setErr] = useState('');

    const onSignUp = () => {
        if (email.length > 0 && userName.length >= 4)
        {
            if (match && password.length >= 6) {
                firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
                    let user = firebase.auth().currentUser;
                    if(!!user)
                    {
                        user.updateProfile({
                            displayName: userName
                          }).catch(function(error) {
                            console.log(error);
                          });
                    }
                    
                    let name = userName;
                    firebase.database().ref('users/' + user.uid).set({
                        projects: '',
                        userName: name,
                        created: String(new Date()),
                        lastLoggIn: String(new Date()),
                    })

                }).catch(function(error) {
                    var errorMessage = error.message;
                    setErr(errorMessage);
                });
            } else {
                setErr("Passwords don't match or are to short")
            }
        } else {
            setErr("Email requierd");
        }
        
    }

    return (
        <View style={styles.container}>
            <Text style={styles.text}>Email:</Text>
            <TextInput 
                textContentType='emailAddress'
                style={styles.emailInput}
                placeholder='Email' 
                value={email}
                onChangeText={(emailIn) => {
                    setEmail(emailIn)
                }}
            />

            <Text style={styles.text}>User:</Text>
            <TextInput 
                textContentType='nickname'
                style={styles.emailInput}
                placeholder='Username' 
                value={userName}
                onChangeText={(userName) => {
                    setUserName(userName)
                }}
            />

            <Text style={styles.text}>Password:</Text>
            {(len < 6 && len !== 0) ? <Text style={styles.textErr}>Password to short must be atlest 6 character long</Text> : null }
            <TextInput 
                textContentType='password'
                secureTextEntry={true}
                style={styles.passwordInput}
                placeholder='Password' 
                value={password}
                onChangeText={(passwordIn) => {
                    setLen(passwordIn.length);
                    setPassword(passwordIn)
                }}
            />
            <Text style={styles.text}>Repeat Password:</Text>
            {(!match) ? <Text style={styles.textErr}>Passwords don't match</Text> : null }
            <TextInput 
                textContentType='password'
                secureTextEntry={true}
                style={styles.passwordInput}
                placeholder='Type password again' 
                value={confirmPassword}
                onChangeText={(password2) => {
                    setPassword2(password2);
                    if (password === password2 || password2.length < 4){
                        setMatch(true);
                    } else {
                        setMatch(false);
                    }
                }}
            />
            {(err.length > 0) ? <Text style={{...styles.textErr, textAlign: "center"}}>{err}</Text> : null }
            <Button  onPress={onSignUp} title='Sign up' />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20
    },
    emailInput: {
        borderWidth: 1,
        padding: 10,
        margin: 5
    },
    passwordInput: {
        borderWidth: 1,
        padding : 10,
        margin: 5,

    },
    text: {
        margin: 10,
        marginBottom: 3,
        fontSize: 15
    },
    textErr: {
        marginBottom: 3,
        marginHorizontal: 10,
        fontSize: 15,
        color: 'red',

    }
})