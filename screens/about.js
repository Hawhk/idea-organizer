import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default About = () => {
    return(
        <View style={styles.container}>
            <Text style={styles.header}>About</Text>
            <View >
                <Text style={styles.text}>
                    This app lets you make projects where you can add your ideas under diffrent categories.
                </Text>
                <Text style={styles.text}>
                    Get started by going to Home and add new projects.
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: "10%",
        paddingTop: 80,
        alignItems: "center"
    },
    text: {
        fontSize: 20,
        margin: 10
    },
    header: {
        fontSize: 30
    }
});