import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import {View, Text, TextInput, StyleSheet, Button} from 'react-native';
import MyButton from '../common/button';
import { FirebaseHOK } from '../config/firebase'

export default FirebaseHOK(LoggIn);

function LoggIn({firebase}) {
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [err, setErr] = useState('');

    const navigation = useNavigation();

    const onLoggIn = () => {
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
            // Handle Errors here.
            // var errorCode = error.code;
            var errorMessage = error.message;
            setErr(errorMessage)
            console.log(errorMessage)
        });
    }

    return (
        <View style={styles.container}>
            <Text style={styles.header}>Welcome to Idea Organizer</Text>
            <Text style={styles.text}>Email:</Text>
            <TextInput 
                textContentType='emailAddress'
                style={styles.emailInput}
                placeholder='Email' 
                value={email}
                onChangeText={(emailIn) => {
                    setEmail(emailIn)
                }}
            />
            <Text style={styles.text}>Password:</Text>
            <TextInput 
                textContentType='password'
                secureTextEntry={true}
                style={styles.passwordInput}
                placeholder='Password' 
                value={password}
                onChangeText={(passwordIn) => {
                    setPassword(passwordIn)
                }}
            />
            {(err.length > 0) ? <Text style={{...styles.textErr, textAlign: "center"}}>{err}</Text> : null }
            <MyButton onPress={onLoggIn} title='Logg in' />
            <Button title='Sign up insted' onPress={() => navigation.navigate('Sign up')} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20
    },
    emailInput: {
        borderWidth: 1,
        padding: 10,
        margin: 5
    },
    passwordInput: {
        borderWidth: 1,
        padding : 10,
        margin: 5,
    },
    text: {
        margin: 10,
        marginBottom: 3,
        fontSize: 15
    },
    textErr: {
        marginBottom: 3,
        marginHorizontal: 10,
        fontSize: 15,
        color: 'red',

    },
    header: {
        fontSize: 20,
        textAlign: 'center',
    }
})