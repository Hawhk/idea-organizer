import React, { useState} from 'react';
import {Text, View, FlatList, StyleSheet, Alert} from 'react-native';
import DialogInput from 'react-native-dialog-input';
import { FirebaseHOK } from '../config/firebase'
import LoggOut from '../common/loggOut'
import Project from '../items/project'
import { MaterialIcons } from '@expo/vector-icons'
import {Picker} from '@react-native-community/picker'

export default FirebaseHOK(Home);
function Home({firebase, navigation}) {

    let userId = firebase.auth().currentUser.uid;
    let user = firebase.database().ref('users/' + userId + '/');
    let keys = [];
    let items = [];
    const [userName, setUserName] = useState('');
    const [render, setRender] = useState(true);
    const [projects, setProjects] = useState({});
    const [isShowingInput, setIsShowingInput] = useState({bool:false});
    const [sort, setSort] = useState(2)

    if(render){
        user.once('value', (snapshot) => {
            setUserName(snapshot.val().userName);
            setProjects(snapshot.val().projects);
        });
        setRender(false);
    }

    const editProject = async (text, item) => { 
        setIsShowingInput({bool:false});
        let uri = `users/${userId}/projects/${item.key}/`;
        text = text.slice(0, 25);
        let update = {};
        update[uri + "name"] = text;
        update[uri + "edited"] = new Date();
        await firebase.database().ref().update(update);
        setRender(true);
    }

    const deleteProject = (item) => {
        let uri = `users/${userId}/projects/${item.key}/`;
        firebase.database().ref(uri).remove();
        setRender(true);
    }

    const editPrompt = (item) => {
        Alert.alert(
            `"${item.name}"`,
            "",
            [
                {
                    text: "Edit",
                    onPress: () => {setIsShowingInput({bool: true, text: "Edit", func: editProject, item: item, input: item.name})},
                },
                {
                    text: "Delete",
                    onPress: () => {deleteProject(item)},
                    style: "red"
                },
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                }
            ],
            { cancelable: false }
        )
    }

    const createProject = (name) => {
        setIsShowingInput({bool:false});
        if(name === "" || name === undefined) {
            //should look and see if other unnamed project exists and just add one to the end
            name = 'Project: ' + Math.round(Math.random() * 1000);
        }
        
        let projectRef = firebase.database().ref('users/' + userId + '/projects/').push();
        let date = String(new Date());
        projectRef.set({
            name: name,
            categories: '',
            added: date,
            edited: date,
        });
        
        setRender(true);
    }

    if (!!projects) {
        keys = Object.keys(projects);
    }
    

    keys.forEach((key) => {
        items.push( {...projects[key], key: key} );
    });

    if(sort === 0) {
        items.sort( (a, b) => { return (new Date(a.added) - new Date(b.added) > 0) ? -1 : 1; })
    } else if (sort === 1) {
        items.sort( (a, b) => { return (new Date(a.edited) - new Date(b.edited) > 0) ? -1 : 1; })
    } else {
        items.sort((a, b) => { 
            let name1 = a.name.toUpperCase();
            let name2 = b.name.toUpperCase();
            
            return (name1 < name2) ? -1 : 1  
        })
    }   

    return (
        <View style={styles.container}>
            <View style={{alignItems: 'center'}}>
                <Text style={{fontSize: 22, marginVertical: "4%" }}>Welcome: {userName}</Text>
                <MaterialIcons name='add' size={40}  style={styles.icon2} onPress={() => setIsShowingInput({bool:true, text: "Set title", func: createProject})}/>
            </View>
            <Picker
                style={{height: 120}}
                selectedValue={sort}
                onValueChange={(itemValue) => setSort(itemValue)}
            >
                <Picker.Item label={"Added"} value={0} />
                <Picker.Item label="Edited" value={1} />
                <Picker.Item label="Name" value={2} />
            </Picker>
            <View style={styles.list}>
                <FlatList
                    onRefresh={() => setRender(true)}
                    refreshing={render}
                    data={items}
                    renderItem={({ item }) => (
                        <Project item={{...item, uid: userId}} pressHandlers={{
                            goTo: () => {navigation.navigate('Project', item)},
                            edit: () => editPrompt(item)
                        }} />
                    )}
                />
                <DialogInput
                    isDialogVisible={isShowingInput.bool}
                    title={isShowingInput.text}
                    hintInput={"title"}
                    initValueTextInput={isShowingInput.input}
                    submitInput={ (inputText) => {isShowingInput.func(inputText, isShowingInput.item)}}
                    closeDialog={ () => {setIsShowingInput({bool:false})}}
                >
                </DialogInput>
            </View>
            <LoggOut/>
            
        </View>
    )
}

let styles = StyleSheet.create({
    list: {
        flex: 1,
        marginTop: 20,
        backgroundColor: "#eee"
    },
    container: {
        flex: 1,
        backgroundColor: "#eee"
    }
})