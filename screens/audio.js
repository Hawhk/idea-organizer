import React, { useState, useEffect } from 'react';
import { Audio } from 'expo-av';
import {Text, StyleSheet, View } from "react-native";
import {FirebaseHOK} from '../config/firebase';
import Button from '../common/button';
import * as FileSystem from 'expo-file-system';
import DialogInput from 'react-native-dialog-input';
import randomId from '../common/random';

// const uniqid = require('uniqid');

export default FirebaseHOK(AudioRecorder)

function AudioRecorder({firebase, route, navigation:{goBack}}) {
    const recordingSettings = {
        android: {
            extension: ".m4a",
            outputFormat: Audio.RECORDING_OPTION_ANDROID_OUTPUT_FORMAT_MPEG_4,
            audioEncoder: Audio.RECORDING_OPTION_ANDROID_AUDIO_ENCODER_AAC,
            sampleRate: 44100,
            numberOfChannels: 2,
            bitRate: 128000,
        },
        ios: {
            extension: ".m4a",
            outputFormat: Audio.RECORDING_OPTION_IOS_OUTPUT_FORMAT_MPEG4AAC,
            audioQuality: Audio.RECORDING_OPTION_IOS_AUDIO_QUALITY_MIN,
            sampleRate: 44100,
            numberOfChannels: 2,
            bitRate: 128000,
            linearPCMBitDepth: 16,
            linearPCMIsBigEndian: false,
            linearPCMIsFloat: false,
        },
    };
    const params = route.params;
    const userId = params.uid;
    const projectsRefPath = params.path;
    const soundObject = new Audio.Sound();
    const refrech = params.refrech;
    const createIdea = params.create;


    const [isRecording, setIsRecording] = useState(false);
    const [recording, setRecording] = useState(null);
    const [sound, setSound] = useState(null);
    const [isPlaying, setIsPlaying] = useState(false);
    const [isDialogVisible, setIsDialogVisible] = useState(false);
    
    useEffect(() => {
        return async () => {
            await soundObject.unloadAsync();
            refrech(true);
        };
    }, []);

    const startRecording = async () => {
        // stop playback
        await Audio.requestPermissionsAsync()

        if (sound !== null) {
            await sound.unloadAsync();
            sound.setOnPlaybackStatusUpdate(null);
            setSound(null);
        }
    
        await Audio.setAudioModeAsync({
            allowsRecordingIOS: true,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
            playsInSilentModeIOS: true,
            shouldDuckAndroid: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            playThroughEarpieceAndroid: false,
            staysActiveInBackground: true,
        });
        const _recording = new Audio.Recording();
        try {
            await _recording.prepareToRecordAsync(recordingSettings);
            setRecording(_recording);
            await _recording.startAsync();
            setIsRecording(true);
        } catch (error) {
            console.log("error while recording:", error);
        }
    };

    const stopRecording = async () => {
        try {
            await recording.stopAndUnloadAsync();
        } catch (error) {
            // Do nothing -- we are already unloaded.
        }
        await Audio.setAudioModeAsync({
            allowsRecordingIOS: false,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
            playsInSilentModeIOS: true,
            playsInSilentLockedModeIOS: true,
            shouldDuckAndroid: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            playThroughEarpieceAndroid: false,
            staysActiveInBackground: true,
        });

        const { sound: _sound, status } = await recording.createNewLoadedSoundAsync(
        {
            isLooping: true,
            isMuted: false,
            volume: 1.0,
            rate: 1.0,
            shouldCorrectPitch: true,
        }
        );
        
        setSound(_sound);
        setIsRecording(false);
    };

    const uploadAudio = async (name) => {
        setIsDialogVisible(false);
        const filename = randomId();
        const uri = recording.getURI();
        try {
            const blob = await new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest();
                xhr.onload = () => {
                    try {
                        resolve(xhr.response);
                    } catch (error) {
                        console.log("error:", error);
                    }
                };
                xhr.onerror = (e) => {
                    console.log(e);
                    reject(new TypeError("Network request failed"));
                };
                xhr.responseType = "blob";
                xhr.open("GET", uri, true);
                xhr.send(null);
            });
            if (blob != null) {
                const uriParts = uri.split(".");
                const fileType = uriParts[uriParts.length - 1];
                firebase
                    .storage()
                    .ref()
                    .child('users/' + userId + '/memos/' + filename + ".m4a")
                    .put(blob, {
                        contentType: `audio/${fileType}`,
                }).then(() => {
                    createIdea('memo', filename, name, '', goBack);
                }).catch((e) => console.log("error:", e));
            } else {
                console.log("erroor with blob");
            }
        } catch (error) {
            console.log("error:", error);
        } 
    };

    const playAudio = async () => {
        if (!isPlaying) {
            setIsPlaying(true);
            let uri = recording.getURI();
            try {
                await soundObject.loadAsync({uri});
                soundObject.setOnPlaybackStatusUpdate((status) => {
                    if (status.didJustFinish){
                        setIsPlaying(false);
                    }
                });
                await soundObject.playAsync();
            } catch (error) {
                console.log("error:", error);
            }
        }
    }

    const toRecord = () => {
        if(isRecording) {
            stopRecording();
        } else {
            startRecording();
        }
    }


    return(
        <View style={styles.container}>
            <Text>Record</Text>
            <Button title={(isRecording) ? 'Recording': 'Record'} onPress={toRecord} />
            <Button title="Play recording" onPress={playAudio} />
            <Button title="Add" onPress={() => setIsDialogVisible(true)} />
            <DialogInput isDialogVisible={isDialogVisible}
                title={"Set title"}
                message={"Set title for recording"}
                hintInput ={"title"}
                submitInput={ (inputText) => {uploadAudio(inputText)} }
                closeDialog={ () => {setIsDialogVisible(false)}}>
            </DialogInput>
        </View>
    )
} 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eee',
    },
});