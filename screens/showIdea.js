import React, { useState, useEffect} from 'react';
import { Image, Text, StyleSheet, TextInput, Alert, View, TouchableOpacity, Keyboard,} from "react-native";
import Button from '../common/button'
import {FirebaseHOK} from '../config/firebase';
import { MaterialIcons } from '@expo/vector-icons'
import { Audio } from 'expo-av';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import {Picker} from '@react-native-community/picker'



export default FirebaseHOK(Idea);

function Idea ({route, firebase, navigation:{goBack}}) {
    
    const params = route.params;
    const item = params.item;
    const categories = params.cat;
    const refrech = params.refrech;
    const createIdea = params.create;
    item.title = String(item.title)
    const url = params.url;
    const soundObject = new Audio.Sound();
    const uri = params.uri;
    const db = firebase.database();
    const [title, setTitle] = useState(item.title);
    const [isPlaying, setIsPlaying] = useState(false);
    const [category, setCategory] = useState(item.catKey);

    useEffect(() => {
        return async () => {
            await soundObject.unloadAsync()
            refrech(true);
        };
    }, []);


    const EditShowsButton = ({text, setFunc, func, type}) => {
        return (
            <View>
                <Text>Change {text[2]}:</Text>
                <TextInput
                    style={styles.textInput}
                    onChangeText={text => setFunc(text)}
                    value={text[0]}
                />
                {
                    (text[0] != text[1]) ?
                    <Button title="Change" onPress={() => func(text[0], type)}/>
                    :
                    null
                }
            </View>
        )
    }

    const changeCat = async (cat) => {
        await createIdea(item.type, item.content, title, cat);
        deleteIdea();
    }

    const EditName = ({type}) => {
        const [text, setText] = useState(item.title);
        const [cat, setCat] = useState("");
        return (
            <TouchableNativeFeedback
                onPress={() => Keyboard.dismiss()}
            >
                <View style={{marginVertical: 20, flex: 2}}>
                    
                    <EditShowsButton text={[text, title, "title"]} setFunc={setText} func={updateItem} type={type} />
                    <Picker
                        style={{flex: 1}}
                        selectedValue={category}
                        onValueChange={(itemValue) => setCategory(itemValue)}
                    >
                        {
                            categories.map((item, index) => {
                                return (<Picker.Item label={item.charAt(0).toUpperCase() + item.slice(1)} value={item} key={String(index)}/>)
                            })
                        }
                        <Picker.Item label="Create new" value={false} />
                    </Picker>
                    {
                        (!category)?
                        (
                            <View>
                                <Text>New category:</Text>
                                <TextInput 
                                    style={styles.textInput}
                                    onChangeText={text => setCat(text)}
                                    value={cat}
                                />
                                <Button title="Create" onPress={() =>changeCat(cat)} />
                            </View>
                        )
                        :
                        (category != item.catKey)?
                        <Button title="Change" onPress={() => changeCat(category)} />
                        :
                        <Button title="Delete" onPress={deletePrompt}/>
                    }
                    
                    
                </View>
            </TouchableNativeFeedback>
        )
    }

    const deletePrompt = () => {
        Alert.alert(
            `Delete "${title}"?`,
            "Do you wish to delete this idea?",
            [
                {
                    text: "Delete",
                    onPress: deleteIdea,
                    style: "red"
                },
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                }
            ],
            { cancelable: false }
        )
    }

    const deleteIdea = async() => {
        await db.ref(uri).remove();
        goBack();
    }

    const updateItem = async(title, type, content=item.content) => {
        title = title.slice(0, 25);
        let data = {
            title: title,
            content: content,
            type: type
        }
        let update = {};
        update[uri] = data;
        await db.ref().update(update);
        item.title = title;
        setTitle(title);
    }

    const playAudio = async () => {
        if(!isPlaying){
            setIsPlaying(true);
            try {
                await soundObject.loadAsync({uri: url});
                soundObject.setOnPlaybackStatusUpdate((status) => {
                    if (status.didJustFinish){
                        setIsPlaying(false);
                    }
                });
                await soundObject.playAsync();
            } catch (error) {
                console.log("error:", error);
            }
        } else {
            console.log("Still playing")
        }
    }

    if (item.type === 'photo') {
        return(
            <View style={styles.container}>
                <Text>{title}</Text>
                <View style={{flex: 1.5}}>
                    <Image
                        style={styles.stretch}
                        source={{
                            uri:url
                        }}
                    />
                </View>
                
                <EditName type={item.type}/>
            </View>
        )
    } else if (item.type === 'memo') {
        return(
            <View style={styles.container}>
                <Text>{title}</Text>
                <TouchableOpacity style={{backgroundColor: "#bbb6"}} onPress={playAudio}>
                    <MaterialIcons name="record-voice-over" size={350} color="black"/>
                </TouchableOpacity>
                <EditName type={item.type}/>
            </View>
        )
    } else if (item.type === 'text') {
        const [text, setText] = useState(item.content);
        return(
            <View style={styles.container}>
                <Text>{title}</Text>
                <TextInput
                    style={styles.textEdit}
                    multiline={true}
                    onChangeText={text => setText(text)}
                    value={text}
                />
               {
                   (text != item.content) ?
                    <Button title="Edit" onPress={() => {updateItem(title, "text", text)}}/>
                    :
                    null
                }
                <EditName type={item.type}/>
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: "center",
    },
    stretch: {
        alignSelf: "center",   
        width: "80%",
        height: "100%",
        resizeMode: 'stretch',
    },
    textInput: {
        borderWidth: 1,
        padding: 10,
        margin: 5,
        width: "70%",
        alignSelf: "center"
    },
    textEdit: {
        flex: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingTop: 10,
        margin: 15
    },
});