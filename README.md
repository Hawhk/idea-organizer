# Idea organizer

This project was developed for a customer in the cours PA1414 at Institute of Technology in 2020. The app was ment to solve the issue of having to remember all your ideas. with this app you can simply create projects and add all your ideas.

## Prerequisites

### System
Latest version of `Node` Js is requierd to compile or continue development of this project.

### Firebase
This project requires you to have a project in [firebase](https://console.firebase.google.com/u/0/)

Your `firebase` project must have the Email sign in enabled, otherwise login will not work.

A `firebase` database and Storage bucket is also required.
####Tutorials: 
[Database](https://firebase.google.com/docs/database/web/start)
[Storage](https://firebase.google.com/docs/storage/web/start)

## Setup
Before starting make sure you have everything setup under Prerequisites.

1. Install Node Js latest version
    [Node js](https://nodejs.org/en/)
2. Open a terminal in the folder `./idea-organizer` and run `npm i`
3. Get your project config for webapp from `firebase` and paste in `config/firebase/apikeys.js` inside of the brackets.
It should look something like this (there may be more data):
    ```
    export default {
        config: {
            apiKey: "apiKey",
            authDomain: "projectId.firebaseapp.com",
            databaseURL: "https://databaseName.firebaseio.com",
            storageBucket: "bucket.appspot.com"
        }
    } 
    ```
4. Set database rules for public in `firebase`

## Run app in develepor mode
If you want to run app in develepor mode just run `npm start`
A webbrowser will pop up here you can scan a QR-code wich will take you to expo-cli app
If app is not downloaded then it will take you to the store where you can download it. 
Then scan again.

## Add to store
If you wish to have your app on the appstore follow tutorial:
[Add to store](https://docs.expo.io/distribution/building-standalone-apps/#3-start-the-build)
