import * as firebase from 'firebase'
import 'firebase/auth'
import ApiKeys from './apiKeys';
// import 'firebase/firestore'

if (!firebase.apps.length) {
    firebase.initializeApp(ApiKeys.config);
}


export default firebase;