import firebase from './firebase'
import { FirebaseProvider, FirebaseHOK } from './context'

export default firebase

export { FirebaseProvider, FirebaseHOK}