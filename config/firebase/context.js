import React, { createContext } from 'react'

const FirebaseContext = createContext({})

export const FirebaseProvider = FirebaseContext.Provider

export const FirebaseConsumer = FirebaseContext.Consumer

  export const FirebaseHOK = Component => props => {
    return (
      <FirebaseConsumer>
        {state => <Component {...props} firebase={state} /> }
      </FirebaseConsumer>
    )
}