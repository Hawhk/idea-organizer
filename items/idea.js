import React, { useState } from 'react';
import { Image, Text, StyleSheet, TouchableOpacity, Alert, View } from "react-native";
import { MaterialIcons } from '@expo/vector-icons'
import { Audio } from '../screens/audio';
import { useNavigation } from '@react-navigation/native';
import {FirebaseHOK} from '../config/firebase';

export default FirebaseHOK(Idea);

function Idea({item, uid, firebase, uri, refrech, cat, create}) {
    
    const navigator = useNavigation();
    const navigate = navigator.navigate;
    let storage = firebase.storage();
    uri += "categories/" + item.catKey + '/ideas/' +  item.key;

    const getUrl = (type, func, fileEnd) => {
        let uri = 'users/' + uid + '/'+ type + '/' + item.content + fileEnd;
        storage.ref(uri).getDownloadURL().then((url) => {
            func(url); 
        }).catch((e) => {
            console.error(e);
        })
    }

    //render
    if (item.type === 'photo') {
        
        const [picUrl, setPicUrl] = useState(null);
        if(typeof picUrl != 'string')
        {   
            getUrl('images', setPicUrl, ".jpeg");
        }
        const data = {item: item, url: picUrl, uri: uri, refrech: refrech, cat: cat, create: create}
        return (
            <TouchableOpacity style={styles.container} onPress={() => navigate('Idea', data)} >
                <Text> {item.title} </Text>
                <Image
                    style={styles.stretch}
                    source={{
                        uri:picUrl
                    }}
                />
            </TouchableOpacity>
        )
    } else if (item.type === 'memo') {
        const [memoUrl, setMemoUrl] = useState(null);
        if(typeof memoUrl != 'string')
        {   
            getUrl('memos', setMemoUrl, ".m4a");
        }
        const data = {item: item, url: memoUrl, uri: uri, refrech: refrech, cat: cat, create: create}
        return (
            <TouchableOpacity style={styles.container} onPress={() => navigate('Idea', data)}>
                <Text style={styles.title}> {item.title} </Text>
                <View style={styles.icon}>
                    <MaterialIcons name="record-voice-over" size={150} color="black" />
                </View>
            </TouchableOpacity>
        )
    } else if (item.type === 'text' ) {
        const data = {item: item, uri: uri, refrech: refrech, cat: cat, create: create}
        return (
            <TouchableOpacity style={styles.container} onPress={() => navigate('Idea', data)}>
                <Text style={styles.title}> {item.title} </Text>
                <View style={styles.icon}>
                    <MaterialIcons name="text-fields" size={150} color="black" />
                </View>
            </TouchableOpacity>
        )
    } 
}

const styles = StyleSheet.create({
    container: {
        width: '45%',
        height: '90%',
        marginHorizontal: '2.5%',
        marginVertical: '5%',
        backgroundColor: "#bbb6",
        paddingTop: "1%",
        alignItems: 'center'
    },
    stretch: {
      width: 150,
      height: 150,
      resizeMode: 'stretch',
      alignSelf: 'center',
      margin: "1%"
    },
    icon: {
        alignSelf: 'center',
    },
    title: {
        
    }
});