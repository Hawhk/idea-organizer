import React, { useState, useEffect} from 'react';
import {Text, StyleSheet, TextInput, View, TouchableWithoutFeedback, Keyboard} from "react-native";
import Button from '../common/button'
import {FirebaseHOK} from '../config/firebase';
import randomId from '../common/random';

export default function TextCreate ({route, navigation:{goBack}}) {

    const params = route.params;
    const refrech = params.refrech;
    const title = params.title;
    const uid = params.uid;
    const createIdea = params.create;
    const [text, setText] = useState('');

    const create = () => {
        createIdea("text", text, title, '', goBack); //fix "''"
    }

    return (
        <TouchableWithoutFeedback
            onPress={() => {Keyboard.dismiss()}}
        >
            <View style={styles.container}>
                <Text>Write here:</Text>
                <TextInput
                    style={styles.textInputs}
                    multiline={true}
                    onChangeText={text => setText(text)}
                    value={text}
                />
                <Button title='Create' onPress={create}/>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: '5%'
    },
    textInputs: {
        flex: 1,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingTop: 10,
        margin: 15
    },
});