import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Projects({item, pressHandlers}) {
    return(
        <View style={styles.container}>
            <TouchableOpacity onPress={() => pressHandlers.goTo(item.key)} style={styles.item}>
                <View style={styles.itemView}>
                    <Text style={styles.itemText}>{item.name}</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => pressHandlers.edit(item)} style={styles.icon}>
                <MaterialIcons 
                    name='edit' 
                    size={25} 
                    color='red' 
                    onPress={() => pressHandlers.edit(item.key)}
                />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 16,
        marginTop: 16,
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 5,
    },
    item: {
        flex: 5,
        flexDirection: 'row',
    },
    itemView: {
        width: '90%',
    },
    itemText: {
        marginLeft: 10,
        fontSize: 20
    },
    icon: {
        flex: 1,
        alignSelf: 'center',
    }
});