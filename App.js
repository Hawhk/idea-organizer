import React from 'react';
import firebase, { FirebaseProvider} from './config/firebase/'
import Navigator from './routes/drawer'
import SignIn from './routes/loggInStack'
// import ApiKeys from './config/firebase/apiKeys';
import loggOut from './common/loggOut';
import { LogBox } from 'react-native';

export default class App extends React.Component {

  
  constructor (props) {
    super(props);
    this.state = {
      loggedIn: false,
      checkedLoggIn: false,
      isAuthenticationReady: false,
    }
    LogBox.ignoreLogs(['Non-serializable', 'Setting a timer']);
    // LogBox.ignoredYellowBox = [];
    firebase.auth().onAuthStateChanged((user) => {
      this.setState({loggedIn: !!user});
      if (!!user) {
        console.log('Logged in');
      } else { 
        console.log('logged Out');
      }
    });
  }

  render() {
    return (
      <FirebaseProvider value={firebase}>
        {
          (this.state.loggedIn) ?
            <Navigator/>
            :
            <SignIn/>
        }
      </FirebaseProvider>
    )
  }
}