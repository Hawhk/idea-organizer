import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Camera } from 'expo-camera';
import { FirebaseHOK } from '../config/firebase'

//not implemented in the code yet
//to much work and no requirement
// only need to add photos so i do it from phone libery

//todo: remove image afater handled.

export default FirebaseHOK(CameraBox);

function CameraBox({close, firebase}) {

    let storage = firebase.storage();

    // console.log(storage);

    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);

    const [picture, setPicture] = useState('');


    let camera;

    useEffect(() => {
        (async () => {
          const { status } = await Camera.requestPermissionsAsync();
          setHasPermission(status === 'granted');
        })();
    });
    
    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    const snap = async() => {
        if(camera) {
            let photo = await camera.takePictureAsync({exif:true});

            // setPicture(photo.uri)
            console.log(photo);
        }
    }

    return (
        <Camera 
            style={styles.camera} 
            type={type}
            ref={ref => {
                camera = ref;
            }}
        >
            <View style={styles.cameraView}>
                <TouchableOpacity
                    style={styles.flip}
                    onPress={() => {
                        setType(
                            type === Camera.Constants.Type.back
                            ? Camera.Constants.Type.front
                            : Camera.Constants.Type.back
                        );
                    }}
                >
                    <MaterialIcons name='flip' size={40} />
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.close}
                    onPress={close}
                >
                    <MaterialIcons name='close' size={40} />
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.close}
                    onPress={snap}
                >
                    <MaterialIcons name='photo-camera' size={40} />
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.close}
                    // onPress={snap}
                >
                    <View>
                    {
                        (picture != '') ?
                        <Image 
                            source={{
                                uri:picture
                            }}
                        />
                        :
                        <Text>No photo</Text>
                    }
                    </View>
                    
                </TouchableOpacity>

            </View>
        </Camera>
    )
}

const styles = StyleSheet.create({
    camera: {
        flex: 1
    },
    cameraView: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'row',
    },
    // text: {
    //     fontSize: 18, 
    //     marginBottom: 10, 
    //     color: 'white' 
    // },
    flip: {
        flex: 0.1,
        alignSelf: 'auto',
        alignItems: 'center',
    },
    close: {
        flex: 0.1,
        alignSelf: 'auto',
        alignItems: 'center',
    }
});