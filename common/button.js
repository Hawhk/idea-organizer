import {Text, StyleSheet, View} from 'react-native';
import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function myButton({onPress, title}) {

    return(
        <View style={styles.button}>
            <TouchableOpacity onPress={onPress}>   
                <Text style={styles.buttonText}>{title}</Text>
            </TouchableOpacity>
        </View>
        
    )
}

const styles = StyleSheet.create({
    button: {
        borderWidth: 1,
        width: 100,
        height: 40 ,
        alignSelf: 'center',
        marginTop: 10,
        backgroundColor:'#fff',
        borderRadius:5,
        justifyContent: "center",
        alignItems: 'center',
        marginBottom: 50
    },
    buttonText: {
        fontSize: 20,
        textAlign: 'center'
    }
})