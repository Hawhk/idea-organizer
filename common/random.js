

export default function random(size = 20) {
    let keys = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM_-'
    let sizeOfKeys = keys.length;
    let toReturn = ''
    for (let i = 0; i < size; i++){
        toReturn += keys[Math.round(Math.random() * sizeOfKeys)];
    }
    return toReturn;
}