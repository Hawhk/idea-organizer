import React, { useState } from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import { FirebaseHOK } from '../config/firebase';

export default FirebaseHOK(Header);
function Header({navigation, title, firebase}) {
    const [test, setTest] = useState(false);
    const openMenu = () => {
        navigation.openDrawer();
    }

    return(
        <View style={styles.header}>
            <MaterialIcons name='menu' size={28} onPress={openMenu} style={styles.icon}/>
            <View>
                <Text style={styles.headerText}>{title}</Text>
            </View>
        </View>
    )

}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 29,
        color: '#333',
        letterSpacing: 1,
    },
    icon: {
        position: 'absolute',
        left: -40
    }, 
    icon2: {
        position: 'absolute',
        right: -30
    }

}) 