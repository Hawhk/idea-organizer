import React from 'react';
import MyButton from './button';
import { FirebaseHOK } from '../config/firebase'


export default FirebaseHOK(LoggOut)

function LoggOut({firebase}) {

    const loggOut = () => {
        firebase.auth().signOut().then(function() {
            console.log('logg Out');
          }).catch(function(error) {
            console.error(error);
          })
    }

    return (
        <MyButton title='Logg out' onPress={loggOut} />
    );
}