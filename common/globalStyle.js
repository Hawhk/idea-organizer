import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        paddingLeft: 20,
    },
    text: {
        // textAlign: "center",
        color: '#333',
        fontSize: 20
    },
    paragraph:{
        marginVertical: 8,
        lineHeight: 20
    }
});