import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack'
import { useNavigation } from '@react-navigation/native';
import Home from '../screens/home'
import Project from '../screens/project'
import ShowIdea from '../screens/showIdea';
import RecordSound from '../screens/audio';
import Header from '../common/header'
import Text from '../items/text'

const Stack = createStackNavigator();

export default function HomeStack () {
    const navigation = useNavigation();
    return(
        <Stack.Navigator>
            <Stack.Screen 
                name="Home" 
                component={Home}
                options={{headerTitle: () => <Header navigation={navigation} title='Idea organizer' />}}
            />
            <Stack.Screen name="Project" component={Project}/>
            <Stack.Screen name="Idea" component={ShowIdea}/>
            <Stack.Screen name="Recorder" component={RecordSound}/>
            <Stack.Screen name="Text" component={Text}/>
        </Stack.Navigator>
    )
}