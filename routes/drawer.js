import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeStack from './homeStack'
import About from '../screens/about';
import Header from '../common/header'


const Drawer = createDrawerNavigator();

export default function AllDrawer() {
    
    return(
        <NavigationContainer>
            <Drawer.Navigator initialRouteName='Home' >
                        <Drawer.Screen name='Home' component={HomeStack}/>
                        <Drawer.Screen name='About' component={About} options={{headerTitle: () => <Header navigation={navigation} title='About' />}}/>
            </Drawer.Navigator>
        </NavigationContainer>
    )
}
