import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native';
import LoggIn from '../screens/loggin'
import SignUp from '../screens/signup'

const Stack = createStackNavigator();

export default function SignIn () {
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Logg In" >
                    {() => <LoggIn/>}
                </Stack.Screen>
                <Stack.Screen name="Sign up">
                    {() => <SignUp/>}
                </Stack.Screen>
            </Stack.Navigator>
        </NavigationContainer>
    )
}